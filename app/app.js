/************************************************
 * KT Node.js Example
 * 
 * Author: Rogger Lacerda 
 * 
 * **********************************************/

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var helmet = require('helmet');                                 // Security
var cors = require('cors');                                     // Security for Cross Origin Resource Sharing
var expressJWT = require('express-jwt');
var jwt = require('jsonwebtoken');                              // Authentication by JSON WEB TOKEN
var consign = require('consign');                               // Load javascript files in folders
var http = require('http');
var https = require('https');
var request = require('request');

var conMongo = require('./config/connectorMongo');              // Connector Mongo
var logger = require('./config/loggerWinston');                 // Logs
var corsOptions = require('./config/corsCrossDomain.js');

var app = express();

// Set global variables
app.connectorMongo = conMongo;
app.models = {};
app.logger = logger;
app.jwt = jwt;
app.cors = cors;
app.corsOptions = corsOptions;
app.http = http;
app.https = https;
app.requestHttp = request;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

// JWT
app.use(expressJWT({secret: 'ktnodetest'}).unless({path:['/api/account/login', '/api/account/create',
    "/api/account/checkIfEmailExists"]}));

//Auto load .js in folders
consign()
  .include('app/models')
  .then('app/controllers')
  .then('app/routes')
  .into(app);

//Config security application
app.use(helmet());
app.use(helmet.hidePoweredBy({ setTo: "KT Node Example | Rogger Lacerda" })); //Hiden Tecnology used into project
app.use(helmet.xframe()); //Filter frame or iframe
app.use(helmet.xssFilter()); //Filter XSS
app.use(helmet.nosniff()); //Filter MIME TYPES
//Resolve problems with cors
app.use(cors());


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.settings.env === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;