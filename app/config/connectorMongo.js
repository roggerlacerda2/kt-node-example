var mongoose = require('mongoose');

var uri = '';
var options = { 
    useMongoClient: true,
    db: { native_parser: true }, 
    server: { poolSize: 5 }   
}

if(process.env.NODE_ENV =='development'){
    uri = 'mongodb://localhost:27017/local';
}
else if(process.env.NODE_ENV =='testing'){
    //uri = 'mongodb://app:nwdb019283.65@localhost:27017/local';
}
else if(process.env.NODE_ENV =='production'){
    //uri = 'mongodb://app:nwdb019283.65@localhost:27017/local';
}else{
    uri = 'mongodb://localhost:27017/local';
    console.log('Environment variable NODE_ENV is wrong or not set on your machine: default is DEVELOPMENT environment');
} 

mongoose.connect(uri, options);

mongoose.connection.on('connected', function () {  
  console.log('Mongoose default connected to ' + uri);
});

mongoose.connection.on('error',function (err) {  
  console.log('Mongoose default connection error: ' + err);
});

mongoose.connection.on('disconnected', function () {  
  console.log('Mongoose default connection disconnected');
});

mongoose.connection.on('open', function () {  
  console.log('Mongoose default connection is open');
});

module.exports = mongoose;