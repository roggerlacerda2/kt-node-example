//Includes
var winston = require('winston');

//Variables
var logger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({filename: './logs/api.log'})
    ]
});
//module exports
module.exports = logger;