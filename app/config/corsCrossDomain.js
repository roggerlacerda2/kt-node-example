//Variables
var whitelist = ['http://localhost:4200'];
var options = {
    origin: function(origin, callback){
        var originIsWhitelisted = whitelist.indexOf(origin) !== -1;
        callback(null, originIsWhitelisted);
    }
};

//module exports
module.exports = options;