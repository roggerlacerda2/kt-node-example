var autoref = require('mongoose-autorefs');
var uuid = require('node-uuid');
var mongoose = require('mongoose');
require('mongoose-uuid2')(mongoose);
var UUID = mongoose.Types.UUID;

module.exports = function(app) {

    var Schema = app.connectorMongo.Schema;
    
    var WebsiteSchema = new Schema({
        _user: {type: Schema.Types.ObjectId, required: true, ref: 'User'}, 
        host_name: {type: String, required : true},
        host_ip: {type: String, required : true}
    },
    {
        timestamps: true
    });

    WebsiteSchema.plugin(autoref, [
        '_user.websites'
    ]);

    WebsiteSchema.set('toObject', {getters: true});
    WebsiteSchema.set('toJSON', {getters: true});

    WebsiteSchema.pre('remove', function(next) {
        
        // Remove all the references that reference the removed document.
        this.model('User').update(
            { },
            { $pull: { "websites": this._id } },
            { multi: true },
            next
        );
    });

    var Website = app.connectorMongo.model('Website', WebsiteSchema);
    
    return Website;
};