var autoref = require('mongoose-autorefs');
var uuid = require('node-uuid');
var mongoose = require('mongoose');
require('mongoose-uuid2')(mongoose);
var UUID = mongoose.Types.UUID;

module.exports = function(app) {

    var Schema = app.connectorMongo.Schema;

    var AccountSchema = new Schema({
        owner: String,
        users: [{ type: Schema.Types.ObjectId, ref: 'User' }]
    },
    {
        timestamps: true
    });

    AccountSchema.plugin(autoref, [
        'users._account'
    ]);

    AccountSchema.set('toObject', {getters: true});
    AccountSchema.set('toJSON', {getters: true});

    AccountSchema.pre('remove', function(next) {

        // Remove all the docs that references the removed document.
        this.model('User').remove({ _account: this._id }, next);
    });

    var Account = app.connectorMongo.model('Account', AccountSchema);
    
    return Account;
};