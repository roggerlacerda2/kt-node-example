var autoref = require('mongoose-autorefs');
var uuid = require('node-uuid');
var mongoose = require('mongoose');
require('mongoose-uuid2')(mongoose);
var UUID = mongoose.Types.UUID;

module.exports = function(app) {

    var Schema = app.connectorMongo.Schema;
    
    var UserSchema = new Schema({
        _account: {type: Schema.Types.ObjectId, required: true, ref: 'Account'}, 
        firstName: {type: String, required : true},
        lastName: {type: String, required : true},
        email: {type: String, unique : true, required : true},
        password:  {type: String, required : true},
        phone: String,
        websites: [{ type: Schema.Types.ObjectId, ref: 'Website' }]
    },
    {
        timestamps: true
    });

    UserSchema.plugin(autoref, [
        '_account.users',
        'websites._user'
    ]);

    UserSchema.set('toObject', {getters: true});
    UserSchema.set('toJSON', {getters: true});

    UserSchema.pre('remove', function(next) {

        // Remove all the docs that references the removed document.
        this.model('Website').remove({ _user: this._id }, next);
        
        // Remove all the references that reference the removed document.
        this.model('Account').update(
            { },
            { $pull: { "users": this._id } },
            { multi: true },
            next
        );
    });

    var User = app.connectorMongo.model('User', UserSchema);
    
    return User;
};