/**
 * Utils.js
 *
 * @description :: Reusable Methods for Controllers
 */

function handleError(res, error) {
    return res.status(error.status).json({ error: error });
};
module.exports.handleError = handleError;