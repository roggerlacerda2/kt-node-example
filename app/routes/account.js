module.exports = function(app) {
    
    var controller = app.app.controllers.account;

    app.route('/api/account/create')
        .post(app.cors(app.corsOptions),controller.create);

    app.route('/api/account/login')
        .post(app.cors(app.corsOptions),controller.login);
    
    app.route('/api/account/checkIfEmailExists')
        .post(app.cors(app.corsOptions),controller.checkIfEmailExists);
};