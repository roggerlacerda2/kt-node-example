module.exports = function(app) {
    
    var controller = app.app.controllers.website;

    app.route('/api/website/create')
        .post(app.cors(app.corsOptions),controller.create);

    app.route('/api/website/getAll/:_user')
        .get(app.cors(app.corsOptions),controller.getAll);
    
    app.route('/api/website/delete/:_id')
        .delete(app.cors(app.corsOptions),controller.delete);
};