var mongoose = require("mongoose");
require('mongoose-uuid2')(mongoose);
var UUID = mongoose.Types.UUID;
var Utils = require('../utils/utils.js');

var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    hash = 'ktnodetest';

function encrypt(text){
    var cipher = crypto.createCipher(algorithm,hash);
    var crypted = cipher.update(text,'utf8','hex');
    crypted += cipher.final('hex');
    return crypted;
}

function decrypt(text){
    var decipher = crypto.createDecipher(algorithm,hash);
    var dec = decipher.update(text,'hex','utf8');
    dec += decipher.final('utf8');
    return dec;
}

module.exports = function(app) {

    var controller = {};

    var Account = app.app.models.account;
    var User = app.app.models.user;

    controller.checkIfEmailExists = function(req, res){
        User.findOne({'email': req.body.email},function(err, user){
            if (err) return Utils.handleError(res, err);

            if(user){
                return res.status(406).json({ msg: "Email already registered" });
            }
            else{
                return res.status(200).json({ msg: "Email not registered" });
            }
        });
    }


    controller.create = function (req, res) {

        var _account = req.body.account;
        var _user = req.body.user;
        _user.password = encrypt(_user.password);

        if (!_account.owner) {
            _account.owner = _user.firstName + ' ' + _user.lastName;
        }

        var new_account = new Account(_account);
        new_account.save(function (err, account) {
            if (err) return Utils.handleError(res, err);

            _user._account = account.id;            
            
            var new_user = new User(_user);
            new_user.save(function (err, user) {
                if (err) return Utils.handleError(res, err);

                if (user) {
                    delete user.password;                                                
                    var token = app.jwt.sign(
                        { 
                            account: account,
                            user: user
                        },
                        'ktnodetest'
                    );
                    return res.status(200).json(token);                
                }
            });  
        });
    };

    controller.login = function (req, res) {
        var email = req.body.email;
        var password = req.body.password;

        User.findOne({email: email, password: encrypt(password)}, '_id _account firstName lastName email phone', function (err, _user) {
            if (err) return Utils.handleError(res, err);

            if (!_user) {
                return res.status(401).json({ msg: "Unauthorized" });
            } else {
                Account.findOne({_id: _user._account}, '_id owner', function (err, _account) {
                    if (err) return Utils.handleError(res, err);
                                 
                    var token = app.jwt.sign(
                        {
                            account: _account.toObject(),
                            user: _user.toObject(),
                        }, 
                        'ktnodetest'
                    );
                    return res.status(201).json(token);
                });
            }
        });
    };

    return controller;
};