var mongoose = require("mongoose");
require('mongoose-uuid2')(mongoose);
var UUID = mongoose.Types.UUID;
var Utils = require('../utils/utils.js');

module.exports = function(app) {

    var controller = {};

    var Website = app.app.models.website;
    var User = app.app.models.user;

    controller.getAll = function(req, res){
        Website.find({_user: req.params._user}, function (err, websites) {
            if (err) return handleError(err);

            return res.status(200).json(websites);
        });
    }


    controller.create = function (req, res) {

        var new_website = new Website(req.body.website);
        new_website.save(function (err, website) {
            if (err) return Utils.handleError(res, err);

            return res.status(200).json({ msg: "Website registered successfully" });
        });
    };

    controller.delete = function (req, res) {
        Website.findOne({_id: req.params._id}, function (err, website) { //Query needed to use the delete cascade
            if (err) return handleError(err);

            if(website){
                website.remove(function (err2) {
                    if (err2) return handleError(err2);

                    res.status(200).json({message: 'Website removed successfully'});
                });
            }
            else{
                res.status(401).json({message: 'Website does not exist'});
            }
        });
    };

    return controller;
};