'use strict';

const chai = require('chai');  
const expect = require('chai').expect;

chai.use(require('chai-http'));

const app = require('../app/app.js'); // Our app

describe('API endpoint /account', function() {  
    this.timeout(10000); // How long to wait for a response (ms)

    before(function() {

    });

    after(function() {

    });

    describe('#create()', function () {
        it('should create new account', function () {
            return chai.request(app)
                .post('/api/account/create')
                .send({ 
                    account: {
                        owner: "Carlos Augusto"                 
                    },
                    user: {
                        firstName: "Carlos",
                        lastName: "Augusto",
                        email: "carlosaugusto@host_email.com",
                        phone: "+55 99 99999 9999",
                        password: "pass12345"
                    }
                 })
                 .then(function(res) {
                    expect(res).to.have.status(200);
                });            
        });
    });

    describe('#checkIfEmailExists()', function () {
        it('should return e-mail not registered', function () {
            return chai.request(app)
                .post('/api/account/checkIfEmailExists')
                .send({email: 'any@host_email.com'})
                .then(function(res) {
                    expect(res).to.have.status(200);
                });              
        });
        it('should return e-mail alredy registered', function () {
            return chai.request(app)
                .post('/api/account/checkIfEmailExists')
                .send({email: 'carlosaugusto@host_email.com'})
                .then(function(res) {
                    throw new Error(res.msg);
                })
                .catch(function(err) {
                    expect(err).to.have.status(406);
                }); 
        });
    });

    describe('#login()', function () {
        it('should return success authentication', function () {
            return chai.request(app)
                .post('/api/account/login')
                .send({ email: 'carlosaugusto@host_email.com', password: 'pass12345' })
                .then(function(res) {
                    expect(res).to.have.status(201);
                });  
        });

        it('should return unauthorized authentication', function () {
            return chai.request(app)
                .post('/api/account/login')
                .send({ email: 'carlosaugusto@host_email.com', password: 'wrong_passwd' })
                .then(function(res) {
                    throw new Error(res.msg);
                })
                .catch(function(err) {
                    expect(err).to.have.status(401);
                });  
        });       
    });
});

describe('API endpoint /website', function() {  
    this.timeout(10000); // How long to wait for a response (ms)

    var token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50Ijp7Il9fdiI6MCwidXBkYXRlZEF0IjoiMjAxOC0wMy0wN1QxOToyNTo1Ny40NTBaIiwiY3JlYXRlZEF0IjoiMjAxOC0wMy0wN1QxOToyNTo1Ny40NTBaIiwib3duZXIiOiJQZXNzb2FsIiwiX2lkIjoiNWFhMDNjYzA3NDQyODM3YzU4ZDRlYzk4IiwidXNlcnMiOltdLCJpZCI6IjVhYTAzY2MwNzQ0MjgzN2M1OGQ0ZWM5OCJ9LCJ1c2VyIjp7Il9fdiI6MCwidXBkYXRlZEF0IjoiMjAxOC0wMy0wN1QxOToyNTo1Ny40NzFaIiwiY3JlYXRlZEF0IjoiMjAxOC0wMy0wN1QxOToyNTo1Ny40NzFaIiwiZmlyc3ROYW1lIjoiUm9nZ2VyIiwibGFzdE5hbWUiOiJMYWNlcmRhIiwiZW1haWwiOiJyb2dnZXJAdGVzdGUiLCJwYXNzd29yZCI6IjkwNDE0ODdjMjAiLCJwaG9uZSI6IjEyMyIsIl9hY2NvdW50IjoiNWFhMDNjYzA3NDQyODM3YzU4ZDRlYzk4IiwiX2lkIjoiNWFhMDNjYzU3NDQyODM3YzU4ZDRlYzk5Iiwid2Vic2l0ZXMiOltdLCJpZCI6IjVhYTAzY2M1NzQ0MjgzN2M1OGQ0ZWM5OSJ9LCJpYXQiOjE1MjA0NTA3NTd9.hSlpwPdEx_5L6xSU_xMiL2ts9KNgpLfydZde6hQWjic";

    before(function() {

    });

    after(function() {

    });

    describe('#create()', function () {
        it('should register a new website', function () {
            return chai.request(app)
                .post('/api/website/create')
                .send({ 
                    website: {
                        _user: "5a1cfb877b7eef1abc958719",
                        host_name: "Teste Host",
                        host_ip: "200.168.169.85",
                    }
                 })
                 .set('Authorization', 'Bearer ' + token)
                 .then(function(res) {
                    expect(res).to.have.status(200);
                });            
        });
    });

    describe('#getAll()', function () {
        var user = {
            id: '5a1cfb877b7eef1abc958719'
        }
        it('should return all the user_s favorite websites', function () {
            return chai.request(app)
                .get('/api/website/getAll/' + user.id)
                .set('Authorization', 'Bearer ' + token)
                .then(function(res) {
                    expect(res).to.have.status(200);
                });              
        });
    });

    describe('#delete()', function () {
        var website = {
            id: '5a1cfb877b7eef1abc958719'
        }
        it('should return website does not exist', function () {
            return chai.request(app)
                .delete('/api/website/delete/5a1c16190e22932058440dc7')
                .set('Authorization', 'Bearer ' + token)
                .then(function(res) {
                    throw new Error(res.msg);
                })
                .catch(function(err) {
                    expect(err).to.have.status(401);
                });  
        });      
    });
});