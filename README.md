# README #

## GeoLocation Test Web API

```
kt-node-example
│   README.md
│   package.json                                                # External modules (dependencies) 
|   .gitignore                                                  # Declaring files and folders to be skipped when uploading or repository to bitbucket
|   
└───app                                                     
|   |   app.js                                              # Main script of the application. Where app global variables, system startup scripts, and connections to MongoDB and external services are defined.
|   └───config                                              # Configurations (BDs, CORS, logger and external services)                             
|   |   
|   └───controllers                                         # Controllers (where requests are processed)
|   |   
|   └───models                                              # Data models for CRUD to be performed on non-relational database collections
|   |   
|   └───public                                              # Public files of the frontend of this API (frontend in WebAPI does not have much interest. We only have one index page)
|   |      
|   └─── routes                                             # RESTfull service route files. Keeps the same folder scheme of controllers for greater organization.
|   |      
|   └─── utils                                              # Reusable functions for controllers
|   |      
|   └─── views                                              # Main page for the API
|   
└─── bin                                                    # Main file for execution
| 
└─── test                                                   # Test files

```

## Installation and development server ([NodeJS] (https://nodejs.org/en/))
```
Install the NodeJS, downloading the executable file on (https://nodejs.org/en/) (according to your OS)
```

```
Install the MongoDB Community, downloading the executable file on (https://www.mongodb.com/download-center#community) (according to your OS)
```

```
Create directories for your database and log files. Open a bash and type: 
$ mkdir c:\data\db
$ mkdir c:\data\log
```

```
Go to C:\Program Files\MongoDB\Server\3.4\bin and execute the app "mongod"
```

```
$ cd kt-node-example
```

```
$ npm install
```

## Build

To run the code in [VisualCode](https://code.visualstudio.com/), simply go to the Debugger menu, click on Start Debugging (with the Launch Program option selected)

* The Web API is going to run on `http://localhost:5000/`.


## Running unit tests

Run `npm test` to execute the unit tests via [Mocha](https://mochajs.org/).


## MongoDB IDE

If you want to use an IDE for MongoDB, donwload Robo 3T (https://robomongo.org/).

## Making Requests to the WebAPI

If you want to simulate requests to the Web API, you can use PostMan (https://www.getpostman.com/)
